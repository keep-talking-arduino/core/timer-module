#include <Arduino.h>
#include <SimpleTimer.h>
#include <CommandHandler.h>
#include <ShiftHandler.h>

#define tonePin 8
SimpleTimer timer;
int clockId;
int clockSpeed = 1000;
CommandHandler cmdHandler;
ShiftHandler shiftHandler;

int timerEvent;
// numbers zero to nine (segment a to g);
bool num[10][8] = {
  {1, 0, 0, 0, 0, 0, 0, 1},
  {1, 1, 0, 0, 1, 1, 1, 1},
  {1, 0, 0, 1, 0, 0, 1, 0},
  {1, 0, 0, 0, 0, 1, 1, 0},
  {1, 1, 0, 0, 1, 1, 0, 0},
  {1, 0, 1, 0, 0, 1, 0, 0},
  {1, 0, 1, 0, 0, 0, 0, 0},
  {1, 0, 0, 0, 1, 1, 1, 1},
  {1, 0, 0, 0, 0, 0, 0, 0},
  {1, 0, 0, 0, 0, 1, 0, 0}
};

int timerMin = 0;
int timerSec = 0;
int timerMillis = 0;

void shiftOutDigit(int digit, int number){
  shiftHandler.clear(0);
  shiftHandler.setValue(0, digit, true);
  shiftHandler.setRegister(1, num[number]);
  shiftHandler.shift();
}

void displayNumber(int number){
  int first = number / 1000;
  int second = (number / 100) %10;
  int third = number % 100 / 10;
  int fourth = number % 10;

  shiftOutDigit(1, first);
  shiftOutDigit(2, second);
  shiftOutDigit(3, third);
  shiftOutDigit(4, fourth);
}

int timeToInt(int minutes, int seconds){
  return (minutes * 100) + seconds;
}

void subBeep(){
  tone(tonePin, 4000, 30);
  //tone(tonePin, 7000, 30);
}
void updateTimer()
{
    if(timerSec == 0){
      if(timerMin == 0){
          // bomb should explode
          Command timeIsUp;
          timeIsUp.Init(MODULE_CURRENT, MODULE_CTRLUNIT, TYPE_NOTIFY, "EXPLODE", "");
          cmdHandler.Send(timeIsUp);
          timer.deleteTimer(clockId);
      }else{
        timerMin--;
        timerSec = 59;
        tone(tonePin, 8000, 80);
        timer.setTimeout(150, subBeep);
      }
    }else{
      timerSec--;
      tone(tonePin, 8000, 80);
      //tone(tonePin, 10000, 80);
      timer.setTimeout(150, subBeep);
    }

}
void setTime(int min, int sec){
  timerMin = min;
  timerSec = sec;
}

void processCommand(Command cmd){
  Serial.println(cmd.ToString());

    //set commands
    if(cmd.GetMessageType().equals(TYPE_SET)){
      if(cmd.GetAction().equals("SETTIME")){
        // set clock time
        setTime(cmd.GetValueOnIndex(1).toInt(), cmd.GetValueOnIndex(2).toInt());
      }
      if(cmd.GetAction().equals("SETSPEED")){
        // change clock speed (for example after strike)
        timer.deleteTimer(clockId);
        clockSpeed = cmd.GetValue().toInt();
        clockId = timer.setInterval(clockSpeed, updateTimer);
      }
      if(cmd.GetAction().equals("STOPMODULE")){
        timer.deleteTimer(clockId);
        clockSpeed = 1000;
        setTime(00, 00);
      }
      if(cmd.GetAction().equals("STOPTIME")){
        timer.deleteTimer(clockId);
      }
      if(cmd.GetAction().equals("STARTTIME")){
        if(cmd.GetValue().toInt() > 0){
          clockSpeed = cmd.GetValue().toInt();
        }
        clockId = timer.setInterval(clockSpeed, updateTimer);
      }
    }
    if(cmd.GetMessageType().equals(TYPE_REQUEST)){
      if(cmd.GetAction().equals("GETTIME")){
        Command replyTime;
        replyTime.Init(MODULE_CURRENT, cmd.GetSender(), TYPE_RESPONSE, "GETTIME", String(timerMin) + "," + String(timerSec));
        cmdHandler.Send(replyTime);
      }
    }
}

void setup()
{
  Serial.begin(57600);

  shiftHandler.init(11, 12, 10);
  cmdHandler.Begin(MODULE_CLOCK, processCommand);
  setTime(00, 00);


  pinMode(tonePin, OUTPUT);
  pinMode(9, OUTPUT);
  analogWrite(9, 250);
  delay(100);
  analogWrite(9, 0);
  delay(150);
  analogWrite(9, 254);
  delay(50);
  analogWrite(9, 0);



}

void loop()
{
  timer.run();
  cmdHandler.Run();
  displayNumber(timeToInt(timerMin, timerSec));
}
